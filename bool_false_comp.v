From CertiCoq.Plugin Require Import CertiCoq.

Definition b := false.

CertiCoq Compile b.
