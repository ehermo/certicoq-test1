#include "values.h"
#include "glue.bool_false_comp.b.h"

int main()
{
  struct thread_info* tinfo = make_tinfo();
  printf("%B", body(tinfo));
  return 0;
}
