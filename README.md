# CertiCoq Test#1

We're testing CertiCoq with a simple extraction. We want to extract ```false : bool```. Thus, we give a definition ```b := false```, and extract it using the CertiCoq plugin with ```CertiCoq Compile b```.

This generates the corresponding C code, glue and headers. Add a ```main()```function in ```bool_false.c```.

To compile the code, first we copy the following files: ```gc_stack.c gc_stack.h values.h config.h values.h```

Compile everything with:

```bash
gcc -o test_bool_false -Wno-everything -O2 -fomit-frame-pointer gc_stack.c bool_false_comp.b.c glue.bool_false_comp.b.c bool_false.c
```
When executing ```test_bool_false```we get a segmentation error.

We've found the same issue with different extractions.
