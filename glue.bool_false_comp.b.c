
struct thread_info;
struct Coq_Init_Datatypes_true_args;
struct Coq_Init_Datatypes_false_args;
struct thread_info {
  unsigned long long *alloc;
  unsigned long long *limit;
  struct heap *heap;
  unsigned long long args[1024];
};

struct Coq_Init_Datatypes_true_args {
};

struct Coq_Init_Datatypes_false_args {
};

extern int printf(signed char *);
extern _Bool is_ptr(unsigned long long);
unsigned int get_unboxed_ordinal(unsigned long long);
unsigned int get_boxed_ordinal(unsigned long long);
unsigned long long make_Coq_Init_Datatypes_bool_true(void);
unsigned long long make_Coq_Init_Datatypes_bool_false(void);
unsigned int get_Coq_Init_Datatypes_bool_tag(unsigned long long);
struct Coq_Init_Datatypes_true_args *get_Coq_Init_Datatypes_true_args(unsigned long long);
struct Coq_Init_Datatypes_false_args *get_Coq_Init_Datatypes_false_args(unsigned long long);
void print_Coq_Init_Datatypes_bool(unsigned long long);
void halt(struct thread_info *, unsigned long long, unsigned long long);
unsigned long long call(struct thread_info *, unsigned long long, unsigned long long);
signed char const lparen_lit[2] = { 40, 0, };

signed char const rparen_lit[2] = { 41, 0, };

signed char const space_lit[2] = { 32, 0, };

signed char const fun_lit[6] = { 60, 102, 117, 110, 62, 0, };

signed char const type_lit[7] = { 60, 116, 121, 112, 101, 62, 0, };

signed char const unk_lit[6] = { 60, 117, 110, 107, 62, 0, };

signed char const prop_lit[7] = { 60, 112, 114, 111, 112, 62, 0, };

unsigned int get_unboxed_ordinal(unsigned long long $v)
{
  return $v >> 1LLU;
}

unsigned int get_boxed_ordinal(unsigned long long $v)
{
  return *((unsigned long long *) $v + 18446744073709551615LLU) & 255LLU;
}

signed char const names_of_Coq_Init_Datatypes_bool[2][6] = { 116, 114, 117,
  101, 0, 0, 102, 97, 108, 115, 101, 0, /* skip 0 */ };

unsigned long long make_Coq_Init_Datatypes_bool_true(void)
{
  return 1;
}

unsigned long long make_Coq_Init_Datatypes_bool_false(void)
{
  return 3;
}

unsigned int get_Coq_Init_Datatypes_bool_tag(unsigned long long $v)
{
  register unsigned int $t;
  $t = get_unboxed_ordinal($v);
  return $t;
}

struct Coq_Init_Datatypes_true_args *get_Coq_Init_Datatypes_true_args(unsigned long long $v)
{
  return (struct Coq_Init_Datatypes_true_args *) 0;
}

struct Coq_Init_Datatypes_false_args *get_Coq_Init_Datatypes_false_args(unsigned long long $v)
{
  return (struct Coq_Init_Datatypes_false_args *) 0;
}

void print_Coq_Init_Datatypes_bool(unsigned long long $v)
{
  register unsigned int $tag;
  $tag = get_Coq_Init_Datatypes_bool_tag($v);
  printf(*(names_of_Coq_Init_Datatypes_bool + $tag));
}

void halt(struct thread_info *$tinfo, unsigned long long $env, unsigned long long $arg)
{
  *((unsigned long long *) (*$tinfo).args + 1LLU) = $arg;
  return;
}

unsigned long long const halt_clo[2] = { &halt, 1LL, };

unsigned long long call(struct thread_info *$tinfo, unsigned long long $clo, unsigned long long $arg)
{
  register unsigned long long *$f;
  register unsigned long long *$envi;
  $f = *((unsigned long long *) $clo + 0LLU);
  $envi = *((unsigned long long *) $clo + 1LLU);
  ((void (*)(struct thread_info *, unsigned long long, unsigned long long)) 
    $f)
    ($tinfo, $envi, $arg);
  return *((unsigned long long *) (*$tinfo).args + 1LLU);
}


