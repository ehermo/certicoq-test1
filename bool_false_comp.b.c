#include "/Users/Punto/Documents/FV/Repos/certicoq/plugin/runtime/prim_floats.h"
#include "/Users/Punto/Documents/FV/Repos/certicoq/plugin/runtime/prim_int63.h"
#include "/Users/Punto/Documents/FV/Repos/certicoq/plugin/runtime/coq_c_ffi.h"

struct stack_frame;
struct thread_info;
struct stack_frame {
  unsigned long long *next;
  unsigned long long *root;
  struct stack_frame *prev;
};

struct thread_info {
  unsigned long long *alloc;
  unsigned long long *limit;
  struct heap *heap;
  unsigned long long args[1024];
  struct stack_frame *fp;
  unsigned long long nalloc;
};

extern struct thread_info *make_tinfo(void);
extern unsigned long long *export(struct thread_info *);
extern unsigned long long body(struct thread_info *);
extern void garbage_collect(struct thread_info *);
extern _Bool is_ptr(unsigned long long);
unsigned long long body(struct thread_info *);
unsigned long long const body_info_102[2] = { 0LL, 0LL, };

unsigned long long body(struct thread_info *$tinfo)
{
  struct stack_frame frame;
  unsigned long long root[0];
  register unsigned long long $bool_false_compdb_101;
  register unsigned long long *$alloc;
  register unsigned long long *$limit;
  register unsigned long long *$args;
  $alloc = (*$tinfo).alloc;
  $limit = (*$tinfo).limit;
  $args = (*$tinfo).args;
  frame.next = root;
  frame.root = root;
  frame.prev = (*$tinfo).fp;
  $bool_false_compdb_101 = 3LLU;
  *($args + 1LLU) = $bool_false_compdb_101;
  (*$tinfo).alloc = $alloc;
  (*$tinfo).limit = $limit;
  return *((unsigned long long *) (*$tinfo).args + 1LLU);
}


